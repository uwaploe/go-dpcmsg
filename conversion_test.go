package dpcmsg

import (
	"bitbucket.org/uwaploe/go-dputil"
	"github.com/gogo/protobuf/proto"
	"testing"
	"time"
)

var pstate = Status_StateType(Status_StateType_value["PROFILE_DOWN"])

// Time-stamp is "Mon Jan 2 15:04:05 -0700 2006"
var status = &Status{
	Time:         proto.Uint64(1136239445000000),
	Profile:      proto.Uint32(42),
	Pressure:     proto.Int32(1234000),
	MotorCurrent: proto.Int32(123),
	State:        &pstate,
	Itemp:        []int32{123, 456, 789},
	Humidity:     proto.Int32(-1),
	Voltage:      proto.Int32(12000),
	Current:      proto.Int32(-456),
	Energy:       proto.Int32(987654),
	RelCharge:    proto.Int32(55),
	Vmon:         proto.Int32(11600),
}

var flntu_dr = &dputil.DataRecord{
	Source: "flntu_1",
	Data: map[string]interface{}{
		"chlaflo": int64(42),
		"ntuflo":  uint64(1234),
	},
}

var ctd_dr = &dputil.DataRecord{
	Source: "ctd_1",
	Data: map[string]interface{}{
		"condwat": float64(33.1),
		"tempwat": float64(7.8),
		"preswat": float64(1012.0),
	},
}

var mmp_dr = &dputil.DataRecord{
	Source: "mmp_1",
	Data: map[string]interface{}{
		"pnum":     int64(42),
		"mode":     "down",
		"pressure": float64(1234.0),
		"current":  int64(123),
	},
}

var clock_dr = &dputil.DataRecord{
	Source: "clock",
	Data: map[string]interface{}{
		"tsend": []int64{1136239445000000, 1136239455000000},
		"trecv": []int64{1136239448000000, 1136239458000000},
	},
}

func TestToDataRecord(t *testing.T) {
	dr := status.ToDataRecord()

	if dr.Source != "profiler" {
		t.Errorf("Bad data-record source field: %v", dr.Source)
	}

	h, m, s := dr.T.UTC().Clock()
	if h != 22 || m != 4 || s != 5 {
		t.Errorf("Time-stamp decode error: %v", dr.T)
	}

	data, ok := dr.Data.(map[string]interface{})
	if !ok {
		t.Errorf("Bad type for Data field: %v", dr.Data)
	}

	rh, ok := data["humidity"].(int32)
	if !ok {
		t.Errorf("Bad type for Humidity field: %v", data["humidity"])
	}

	if rh != -1 {
		t.Errorf("Bad value for Humidity field: %v", rh)
	}

	vmon, ok := data["vmon"].(int32)
	if !ok {
		t.Errorf("Bad type for Vmon field: %v", data["vmon"])
	}

	if vmon != 11600 {
		t.Errorf("Bad value for Vmon field: %v", rh)
	}

	state, ok := data["state"].(string)
	if !ok {
		t.Errorf("Bad type for State field: %v", data["state"])
	}

	if state != "profile_down" {
		t.Errorf("Bad value for State field: %v", data["state"])
	}
}

func TestFromDataRecord(t *testing.T) {
	flntu_dr.T, _ = time.Parse(time.UnixDate, "Mon Jan 2 22:04:05 UTC 2006")
	msg := FlntuFromDataRecord(flntu_dr)

	if msg.GetTime() != uint64(1136239445000000) {
		t.Errorf("Bad time conversion: %v", msg.GetTime())
	}

	if msg.GetChlaflo() != 42 || msg.GetNtuflo() != 1234 {
		t.Errorf("Bad data conversion: %v", msg)
	}

	ctd_dr.T, _ = time.Parse(time.UnixDate, "Mon Jan 2 22:04:05 UTC 2006")
	msg2 := CtdFromDataRecord(ctd_dr)

	if msg2.GetPreswat() != float32(1012.0) {
		t.Errorf("Bad data conversion: %v", msg2)
	}

	mmp_dr.T, _ = time.Parse(time.UnixDate, "Mon Jan 2 22:04:05 UTC 2006")
	msg3 := StatusFromDataRecord(mmp_dr)

	if msg3.GetState() != Status_PROFILE_DOWN {
		t.Errorf("Bad data conversion: %v", msg3)
	}

	msg4 := ClockFromDataRecord(clock_dr)
	if msg4.Tsend[0] != int64(1136239445000000) {
		t.Errorf("Wrong value for Clock.tsend[0]: %#v", msg4)
	}
}
