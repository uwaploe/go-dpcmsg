package dpcmsg

import (
	"gopkg.in/yaml.v2"
	"testing"
)

var input = `
- dir: DIVE
  depth: 5
  maxtime: 60
  tstart: 600
  exclude:
    - acs_1
  pattern: basic
- &P
  dir: RISE
  depth: 2
  maxtime: 60
  tstart: 600
  pattern: basic
- dir: 2
  maxtime: 180
- dir: DIVE
  depth: 5
  maxtime: 60
  tstart: 600
  pattern: basic
- dir: DOCKING
- dir: foo
  depth: 1
  maxtime: 60
- *P
`

func TestYaml(t *testing.T) {
	var (
		data []map[string]interface{}
		prf  *Profile
	)

	err := yaml.Unmarshal([]byte(input), &data)
	if err != nil {
		t.Fatalf("Unmarshal error: %v\n", err)
	}

	prf = ProfileFromInterface(data[0])
	if prf.GetDir() != Profile_DIVE {
		t.Errorf("Decode error: %v\n", prf)
	}
	if prf.GetPattern() != "basic" {
		t.Errorf("Decode error: %v\n", prf)
	}

	prf = ProfileFromInterface(data[2])
	if prf.GetDir() != Profile_STATIONARY {
		t.Errorf("Decode error: %v\n", prf)
	}

	prf = ProfileFromInterface(data[4])
	if prf.GetMaxtime() != 0 {
		t.Errorf("Decode error: %v\n", prf)
	}
	if prf.GetPattern() != "" {
		t.Errorf("Non-empty pattern: %v\n", prf)
	}

	prf = ProfileFromInterface(data[5])
	if prf != nil {
		t.Errorf("Invalid profile not detected: %v\n", prf)
	}

	prf = ProfileFromInterface(data[6])
	if prf.GetMaxtime() != 60 {
		t.Errorf("Decode error: %v\n", prf)
	}
}
