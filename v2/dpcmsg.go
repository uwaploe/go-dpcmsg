// This package is part of the Deep Profiler project and defines
// various message formats used by the DPC to transmit data over
// the inductive modem link.
package dpcmsg

import (
	"errors"
	fmt "fmt"
	reflect "reflect"
	"strings"
	"time"

	dputil "bitbucket.org/uwaploe/go-dputil"
	mpc "bitbucket.org/uwaploe/go-mpc"
	yaml "gopkg.in/yaml.v3"
)

//go:generate ./gen.sh

// Entry for the profiles queue, combines a profile
// definition with a list of excluded sensors.
type ProfileMessage struct {
	// Profile configuration
	Cfg *mpc.ProfileConfig
	// List of excluded sensors
	Exclude []string
}

var mpc_states = map[string]string{
	"up":         "PROFILE_UP",
	"down":       "PROFILE_DOWN",
	"stationary": "STATIONARY",
	"docking":    "DOCKING",
	"docked":     "DOCKED",
	"stopped":    "STOPPED",
}

var ErrDataRecord = errors.New("Invalid DataRecord format")

func toInt64(raw interface{}) int64 {
	v := reflect.ValueOf(raw)
	switch v.Kind() {
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return v.Int()
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		return int64(v.Uint())
	}

	return 0
}

func toUint32(raw interface{}) uint32 {
	v := reflect.ValueOf(raw)
	switch v.Kind() {
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return uint32(v.Int())
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		return uint32(v.Uint())
	}

	return 0
}

// Create a scaled int32 from a float32 or float64
func scaleVal(v interface{}, scale float32) int32 {
	switch x := v.(type) {
	case float32:
		return int32(x * scale)
	case float64:
		return int32(float32(x) * scale)
	}

	return 0
}

// Create a new DataRecord from a Clock message
func (c *Clock) MarshalDataRecord() *dputil.DataRecord {
	d := make(map[string]interface{})
	tstamp := time.Unix(c.Tsend[0]/1000000, (c.Tsend[0]%1000000)*1000)
	d["trecv"] = c.Trecv
	d["tsend"] = c.Tsend
	return &dputil.DataRecord{Source: "clock", T: tstamp, Data: d}
}

// Create a new Clock message from a DataRecord
func (c *Clock) UnmarshalDataRecord(rec *dputil.DataRecord) error {
	if rec.Source != "clock" {
		return ErrDataRecord
	}

	dr := rec.Data.(map[string]interface{})
	c.Tsend = dr["tsend"].([]int64)
	c.Trecv = dr["trecv"].([]int64)

	return nil
}

// Create a new "profiler" DataRecord from a Status message
func (s *Status) MarshalDataRecord() *dputil.DataRecord {
	d := make(map[string]interface{})
	state := int32(s.GetState())
	d["state"] = strings.ToLower(Status_StateType_name[state])
	d["profile"] = s.GetProfile()
	// The profiler data record stores the pressure value as an
	// integer, dbars * 1000. This is in contrast to the mmp
	// data record.
	d["pressure"] = s.GetPressure()
	d["voltage"] = s.GetVoltage()
	d["current"] = s.GetCurrent()
	d["itemp"] = s.GetItemp()
	d["humidity"] = s.GetHumidity()
	d["energy"] = s.GetEnergy()
	d["motor_current"] = s.GetMotorCurrent()
	d["rel_charge"] = s.GetRelCharge()
	d["vmon"] = s.GetVmon()
	d["mpc_time"] = s.GetMpcTime()
	d["speed"] = s.GetSpeed()
	d["duty"] = s.GetDuty()
	d["ipr"] = s.GetIpr()
	// Convert microseconds to seconds and nanoseconds
	t := int64(s.GetTime())
	tstamp := time.Unix(t/1000000, (t%1000000)*1000)

	return &dputil.DataRecord{Source: "profiler", T: tstamp, Data: d}
}

const (
	MmpPrScale    float32 = 1000
	MmpSpeedScale float32 = 1000
)

// Create a new Status message from an "mmp" DataRecord
func (s *Status) UnmarshalDataRecord(rec *dputil.DataRecord) error {
	if rec.Source != "mmp_1" {
		return ErrDataRecord
	}

	s.Time = uint64(rec.T.UnixNano()) / 1000
	s.MpcTime = uint64(rec.T.Unix())
	dr := rec.Data.(map[string]interface{})
	s.Profile = uint32(toInt64(dr["pnum"]))
	s.Pressure = scaleVal(dr["pressure"], MmpPrScale)
	s.MotorCurrent = int32(toInt64(dr["current"]))
	if v, ok := dr["speed"]; ok {
		s.Speed = scaleVal(v, MmpSpeedScale)
	}
	if v, ok := dr["duty"]; ok {
		s.Duty = uint32(toInt64(v))
	}
	state, ok := mpc_states[dr["mode"].(string)]
	if !ok {
		state = strings.ToUpper(dr["mode"].(string))
	}
	s.State = Status_StateType(Status_StateType_value[state])
	return nil
}

func (s *Status) UpdateTime(t time.Time) {
	s.Time = uint64(t.UnixNano()) / 1000
}

const (
	CondwatScale float32 = 10000
	TempwatScale         = 1000
	PreswatScale         = 1000
)

// Create a new DataRecord from a CtdData message
func (c *CtdData) MarshalDataRecord() *dputil.DataRecord {
	s := make(map[string]interface{})
	s["condwat"] = float32(c.GetCondwat()) / CondwatScale
	s["tempwat"] = float32(c.GetTempwat()) / TempwatScale
	s["preswat"] = float32(c.GetPreswat()) / PreswatScale

	// Convert microseconds to seconds and nanoseconds
	t := int64(c.GetTime())
	tstamp := time.Unix(t/1000000, (t%1000000)*1000)

	return &dputil.DataRecord{Source: "ctd_1", T: tstamp, Data: s}
}

// Create a new CtdData message from a "ctd" DataRecord
func (c *CtdData) UnmarshalDataRecord(rec *dputil.DataRecord) error {
	if rec.Source != "ctd_1" {
		return ErrDataRecord
	}

	c.Time = uint64(rec.T.UnixNano()) / 1000
	dr := rec.Data.(map[string]interface{})
	c.Condwat = scaleVal(dr["condwat"], CondwatScale)
	c.Tempwat = scaleVal(dr["tempwat"], TempwatScale)
	c.Preswat = scaleVal(dr["preswat"], PreswatScale)

	return nil
}

const (
	DoconcsScale float32 = 100
	TScale               = 100
)

// Create a new DataRecord from an OptodeData message
func (o *OptodeData) MarshalDataRecord() *dputil.DataRecord {
	s := make(map[string]interface{})
	s["doconcs"] = float32(o.GetDoconcs()) / DoconcsScale
	s["t"] = float32(o.GetT()) / TScale

	// Convert microseconds to seconds and nanoseconds
	t := int64(o.GetTime())
	tstamp := time.Unix(t/1000000, (t%1000000)*1000)

	return &dputil.DataRecord{Source: "optode_1", T: tstamp, Data: s}
}

// Create a new OptodeData message from an "optode" DataRecord
func (o *OptodeData) UnmarshalDataRecord(rec *dputil.DataRecord) error {
	if rec.Source != "optode_1" {
		return ErrDataRecord
	}

	o.Time = uint64(rec.T.UnixNano()) / 1000
	dr := rec.Data.(map[string]interface{})
	o.Doconcs = scaleVal(dr["doconcs"], DoconcsScale)
	o.T = scaleVal(dr["t"], TScale)

	return nil
}

const AcmScale float32 = 100

// Create a new DataRecord from an AcmData message
func (a *AcmData) MarshalDataRecord() *dputil.DataRecord {
	s := make(map[string]interface{})
	s["va"] = float32(a.GetVa()) / AcmScale
	s["vb"] = float32(a.GetVb()) / AcmScale
	s["vc"] = float32(a.GetVc()) / AcmScale
	s["vd"] = float32(a.GetVd()) / AcmScale
	s["hx"] = float32(a.GetHx()) / AcmScale
	s["hy"] = float32(a.GetHy()) / AcmScale
	s["hz"] = float32(a.GetHz()) / AcmScale
	s["tx"] = float32(a.GetTx()) / AcmScale
	s["ty"] = float32(a.GetTy()) / AcmScale

	// Convert microseconds to seconds and nanoseconds
	t := int64(a.GetTime())
	tstamp := time.Unix(t/1000000, (t%1000000)*1000)

	return &dputil.DataRecord{Source: "acm_1", T: tstamp, Data: s}
}

// Create a new AcmData message from an "acm" DataRecord
func (a *AcmData) UnmarshalDataRecord(rec *dputil.DataRecord) error {
	if rec.Source != "acm_1" {
		return ErrDataRecord
	}

	a.Time = uint64(rec.T.UnixNano()) / 1000
	dr := rec.Data.(map[string]interface{})

	a.Va = scaleVal(dr["va"], AcmScale)
	a.Vb = scaleVal(dr["vb"], AcmScale)
	a.Vc = scaleVal(dr["vc"], AcmScale)
	a.Vd = scaleVal(dr["vd"], AcmScale)
	a.Hx = scaleVal(dr["hx"], AcmScale)
	a.Hy = scaleVal(dr["hy"], AcmScale)
	a.Hz = scaleVal(dr["hz"], AcmScale)
	a.Tx = scaleVal(dr["tx"], AcmScale)
	a.Ty = scaleVal(dr["ty"], AcmScale)

	return nil
}

// Create a new DataRecord from an FlntuData message
func (f *FlntuData) MarshalDataRecord() *dputil.DataRecord {
	s := make(map[string]interface{})
	s["chlaflo"] = f.GetChlaflo()
	s["ntuflo"] = f.GetNtuflo()

	// Convert microseconds to seconds and nanoseconds
	t := int64(f.GetTime())
	tstamp := time.Unix(t/1000000, (t%1000000)*1000)

	return &dputil.DataRecord{Source: "flntu_1", T: tstamp, Data: s}
}

// Create a new FlntuData message from an "flntu" DataRecord
func (f *FlntuData) UnmarshalDataRecord(rec *dputil.DataRecord) error {
	if rec.Source != "flntu_1" {
		return ErrDataRecord
	}

	f.Time = uint64(rec.T.UnixNano()) / 1000
	dr := rec.Data.(map[string]interface{})
	f.Chlaflo = int32(toInt64(dr["chlaflo"]))
	f.Ntuflo = int32(toInt64(dr["ntuflo"]))

	return nil
}

// Create a new DataRecord from an FlcdData message
func (f *FlcdData) MarshalDataRecord() *dputil.DataRecord {
	s := make(map[string]interface{})
	s["cdomflo"] = f.GetCdomflo()

	// Convert microseconds to seconds and nanoseconds
	t := int64(f.GetTime())
	tstamp := time.Unix(t/1000000, (t%1000000)*1000)

	return &dputil.DataRecord{Source: "flcd_1", T: tstamp, Data: s}
}

// Create a new FlcdData message from an "flcd" DataRecord
func (f *FlcdData) UnmarshalDataRecord(rec *dputil.DataRecord) error {
	if rec.Source != "flcd_1" {
		return ErrDataRecord
	}

	f.Time = uint64(rec.T.UnixNano()) / 1000
	dr := rec.Data.(map[string]interface{})
	f.Cdomflo = int32(toInt64(dr["cdomflo"]))

	return nil
}

// Create one or more new DataRecords from a SensorData message
func (cont *SensorData) ToDataRecords() []*dputil.DataRecord {
	recs := make([]*dputil.DataRecord, 0)

	for _, rec := range cont.GetCtd() {
		recs = append(recs, rec.MarshalDataRecord())
	}

	for _, rec := range cont.GetOptode() {
		recs = append(recs, rec.MarshalDataRecord())
	}

	for _, rec := range cont.GetAcm() {
		recs = append(recs, rec.MarshalDataRecord())
	}

	for _, rec := range cont.GetFlntu() {
		recs = append(recs, rec.MarshalDataRecord())
	}

	for _, rec := range cont.GetFlcd() {
		recs = append(recs, rec.MarshalDataRecord())
	}

	for _, rec := range cont.GetBatt() {
		recs = append(recs, rec.MarshalDataRecord())
	}

	return recs
}

// Create a new SensorData message from one or more DataRecords
func SensorFromDataRecords(recs []*dputil.DataRecord) *SensorData {
	sd := &SensorData{}
	for _, rec := range recs {
		switch rec.Source {
		case "ctd_1":
			ctd := &CtdData{}
			ctd.UnmarshalDataRecord(rec)
			sd.Ctd = append(sd.Ctd, ctd)
		case "optode_1":
			op := &OptodeData{}
			op.UnmarshalDataRecord(rec)
			sd.Optode = append(sd.Optode, op)
		case "acm_1":
			acm := &AcmData{}
			acm.UnmarshalDataRecord(rec)
			sd.Acm = append(sd.Acm, acm)
		case "flntu_1":
			flntu := &FlntuData{}
			flntu.UnmarshalDataRecord(rec)
			sd.Flntu = append(sd.Flntu, flntu)
		case "flcd_1":
			flcd := &FlcdData{}
			flcd.UnmarshalDataRecord(rec)
			sd.Flcd = append(sd.Flcd, flcd)
		case "battavg":
			b := &BatterySummary{}
			b.UnmarshalDataRecord(rec)
			sd.Batt = append(sd.Batt, b)
		}
	}

	return sd
}

// Create a new ProfileMessage struct from a Profile struct.  This is
// the internal format used on the DPC.
//
// The ProfileMessage struct will be stored in a queue, the second return
// value defines how the struct should be added to the queue; replace,
// prepend, or append, and the third value is the pattern name.
func (p *Profile) ToProfileMessage() (*ProfileMessage, Profile_Queueing, string) {
	prErr := p.GetErr()
	if prErr == 0 {
		prErr = 3
	}
	cfg := mpc.ProfileConfig{
		StartTime:  int64(p.GetTstart()),
		StopCheck:  5,
		ShallowErr: uint16(prErr),
		DeepErr:    uint16(prErr),
		TimeLimit:  uint16(p.GetMaxtime()),
		Requeue:    p.GetRequeue(),
	}
	switch p.GetDir() {
	case Profile_DIVE:
		cfg.Direction = mpc.PROFILE_DOWN
		cfg.Deep = uint16(p.GetDepth())
		cfg.Shallow = 0
	case Profile_RISE:
		cfg.Direction = mpc.PROFILE_UP
		cfg.Shallow = uint16(p.GetDepth())
		cfg.Deep = 6000
	case Profile_STATIONARY:
		cfg.Direction = mpc.PROFILE_STATIONARY
	case Profile_DOCKING:
		cfg.Direction = mpc.PROFILE_DOCKING
	}

	msg := ProfileMessage{Cfg: &cfg, Exclude: p.GetExclude()}

	return &msg, p.GetQueueing(), p.GetPattern()
}

func (p *Profile) UnmarshalYAML(node *yaml.Node) error {
	m := make(map[string]interface{})
	if err := node.Decode(&m); err != nil {
		return err
	}

	pp := ProfileFromInterface(m)
	if pp == nil {
		return fmt.Errorf("Invalid format: %q", node.Value)
	}

	*p = *pp

	return nil
}

// Create a Profile struct from a map of empty interfaces. The primary
// use of this function is to allow the use of strings for enum values.
func ProfileFromInterface(raw map[string]interface{}) *Profile {
	p := Profile{Queueing: Profile_APPEND}
	p.Exclude = make([]string, 0)

	for key, val := range raw {
		switch key {
		case "dir":
			switch v := val.(type) {
			case string:
				x, ok := Profile_ProfileType_value[v]
				if !ok {
					return nil
				}
				p.Dir = Profile_ProfileType(x)
			case int:
				p.Dir = Profile_ProfileType(v)
			}
		case "queueing":
			switch v := val.(type) {
			case string:
				x, ok := Profile_Queueing_value[v]
				if !ok {
					return nil
				}
				p.Queueing = Profile_Queueing(x)
			case int32:
				p.Queueing = Profile_Queueing(v)
			}
		case "exclude":
			for _, e := range val.([]interface{}) {
				p.Exclude = append(p.Exclude, e.(string))
			}
		case "depth":
			p.Depth = int32(val.(int))
		case "maxtime":
			p.Maxtime = int32(val.(int))
		case "tstart":
			p.Tstart = int32(val.(int))
		case "requeue":
			p.Requeue = uint32(val.(int))
		case "pattern":
			p.Pattern = val.(string)
		case "err", "error":
			p.Err = uint32(val.(int))
		}
	}

	return &p
}

const BattScale float32 = 1000

// Create a new DataRecord from a BatterySummary message
func (b *BatterySummary) MarshalDataRecord() *dputil.DataRecord {
	s := make(map[string]interface{})
	s["count"] = b.Count
	s["capacity"] = b.Capacity
	s["voltage"] = float32(b.GetVoltage()) / BattScale
	s["amperage"] = float32(b.GetAmperage()) / BattScale

	// Convert microseconds to seconds and nanoseconds
	t := int64(b.GetTime())
	tstamp := time.Unix(t/1000000, (t%1000000)*1000)

	return &dputil.DataRecord{Source: "battavg", T: tstamp, Data: s}
}

// Create a new BatterySummary message from a DataRecord
func (b *BatterySummary) UnmarshalDataRecord(rec *dputil.DataRecord) error {
	if rec.Source != "battavg" {
		return ErrDataRecord
	}

	b.Time = uint64(rec.T.UnixNano()) / 1000
	dr := rec.Data.(map[string]interface{})

	b.Count = toUint32(dr["count"])
	b.Capacity = toUint32(dr["capacity"])
	b.Voltage = scaleVal(dr["voltage"], BattScale)
	b.Amperage = scaleVal(dr["amperage"], BattScale)

	return nil
}
