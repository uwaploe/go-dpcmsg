module bitbucket.org/uwaploe/go-dpcmsg/v2

go 1.17

require (
	bitbucket.org/uwaploe/go-dputil v1.4.1
	bitbucket.org/uwaploe/go-mpc v1.3.1
	google.golang.org/protobuf v1.27.1
	gopkg.in/yaml.v3 v3.0.1
)
