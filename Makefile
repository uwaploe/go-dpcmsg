#
# Makefile for DPC protocol buffer code generation.
#
PB_SOURCE := profiler.proto
PY_SOURCE := $(PB_SOURCE:.proto=_pb2.py)
GO_SOURCE := $(PB_SOURCE:.proto=.pb.go)
PB_PATH := $(GOPATH)/src:$(GOPATH)/src/github.com/gogo/protobuf/protobuf:.

all: go

clean:
	rm -f $(GO_SOURCE) $(PY_SOURCE) *pb_test.go

go: $(GO_SOURCE)

python: $(PY_SOURCE)

test: go
	go test

%_pb2.py: %.proto
	protoc -I$(PB_PATH) --python_out=. $<

%.pb.go: %.proto
	protoc -I$(PB_PATH) --gogo_out=. $<
