// This package is part of the Deep Profiler project and defines
// various message formats used by the DPC. Most of the formats
// are defined in the ProtocolBuffer file.
package dpcmsg

import (
	"bitbucket.org/uwaploe/go-dputil"
	"bitbucket.org/uwaploe/go-mpc"
	"github.com/gogo/protobuf/proto"
	"strings"
	"time"
)

// Entry for the profiles queue, combines a profile
// definition with a list of excluded sensors.
type ProfileMessage struct {
	// Profile configuration
	Cfg *mpc.ProfileConfig
	// List of excluded sensors
	Exclude []string
}

var mpc_states = map[string]string{
	"up":         "PROFILE_UP",
	"down":       "PROFILE_DOWN",
	"stationary": "STATIONARY",
	"docking":    "DOCKING",
	"docked":     "DOCKED",
	"stopped":    "STOPPED",
}

// Create a new DataRecord from a Clock message
func (rec *Clock) ToDataRecord() *dputil.DataRecord {
	d := make(map[string]interface{})
	tstamp := time.Unix(rec.Tsend[0]/1000000, (rec.Tsend[0]%1000000)*1000)
	d["trecv"] = rec.Trecv
	d["tsend"] = rec.Tsend
	return &dputil.DataRecord{Source: "clock", T: tstamp, Data: d}
}

// Create a new Clock message from a DataRecord
func ClockFromDataRecord(rec *dputil.DataRecord) *Clock {
	if rec.Source != "clock" {
		return nil
	}

	dr := rec.Data.(map[string]interface{})
	return &Clock{
		Tsend: dr["tsend"].([]int64),
		Trecv: dr["trecv"].([]int64),
	}
}

// Create a new "profiler" DataRecord from a Status message
func (rec *Status) ToDataRecord() *dputil.DataRecord {
	s := make(map[string]interface{})
	state := int32(rec.GetState())
	s["state"] = strings.ToLower(Status_StateType_name[state])
	s["profile"] = rec.GetProfile()
	s["pressure"] = rec.GetPressure()
	s["voltage"] = rec.GetVoltage()
	s["current"] = rec.GetCurrent()
	s["itemp"] = rec.GetItemp()
	s["humidity"] = rec.GetHumidity()
	s["energy"] = rec.GetEnergy()
	s["motor_current"] = rec.GetMotorCurrent()
	s["rel_charge"] = rec.GetRelCharge()
	s["vmon"] = rec.GetVmon()
	// Convert microseconds to seconds and nanoseconds
	t := int64(rec.GetTime())
	tstamp := time.Unix(t/1000000, (t%1000000)*1000)

	return &dputil.DataRecord{Source: "profiler", T: tstamp, Data: s}
}

// Create a new Status message from an "mmp" DataRecord
func StatusFromDataRecord(rec *dputil.DataRecord) *Status {
	if rec.Source != "mmp_1" {
		return nil
	}

	msg := &Status{Time: proto.Uint64(uint64(rec.T.UnixNano()) / 1000)}

	dr := rec.Data.(map[string]interface{})
	switch t := dr["pnum"].(type) {
	case int64:
		msg.Profile = proto.Uint32(uint32(t))
	case uint64:
		msg.Profile = proto.Uint32(uint32(t))
	}

	msg.Pressure = proto.Int32(int32(dr["pressure"].(float64) * 1000))

	switch t := dr["current"].(type) {
	case int64:
		msg.MotorCurrent = proto.Int32(int32(t))
	case uint64:
		msg.MotorCurrent = proto.Int32(int32(t))
	}

	state := mpc_states[dr["mode"].(string)]
	s := Status_StateType(Status_StateType_value[state])
	msg.State = &s
	return msg
}

// Create a new DataRecord from a CtdData message
func (rec *CtdData) ToDataRecord() *dputil.DataRecord {
	s := make(map[string]interface{})
	s["condwat"] = rec.GetCondwat()
	s["tempwat"] = rec.GetTempwat()
	s["preswat"] = rec.GetPreswat()
	if tempctd := rec.GetTempctd(); tempctd != 0 {
		s["tempctd"] = tempctd
	}

	// Convert microseconds to seconds and nanoseconds
	t := int64(rec.GetTime())
	tstamp := time.Unix(t/1000000, (t%1000000)*1000)

	return &dputil.DataRecord{Source: "ctd_1", T: tstamp, Data: s}
}

// Create a new CtdData message from a "ctd" DataRecord
func CtdFromDataRecord(rec *dputil.DataRecord) *CtdData {
	if rec.Source != "ctd_1" {
		return nil
	}
	// Save some typing
	pf32 := proto.Float32

	msg := &CtdData{Time: proto.Uint64(uint64(rec.T.UnixNano()) / 1000)}
	dr := rec.Data.(map[string]interface{})
	msg.Condwat = pf32(float32(dr["condwat"].(float64)))
	msg.Tempwat = pf32(float32(dr["tempwat"].(float64)))
	msg.Preswat = pf32(float32(dr["preswat"].(float64)))
	// Tempctd is only present in Level-0 data records
	if _, l0 := dr["tempctd"]; l0 {
		msg.Tempctd = pf32(float32(dr["tempctd"].(float64)))
	}

	return msg
}

// Create a new DataRecord from an OptodeData message
func (rec *OptodeData) ToDataRecord() *dputil.DataRecord {
	s := make(map[string]interface{})
	s["doconcs"] = rec.GetDoconcs()
	s["t"] = rec.GetT()

	// Convert microseconds to seconds and nanoseconds
	t := int64(rec.GetTime())
	tstamp := time.Unix(t/1000000, (t%1000000)*1000)

	return &dputil.DataRecord{Source: "optode_1", T: tstamp, Data: s}
}

// Create a new OptodeData message from an "optode" DataRecord
func OptodeFromDataRecord(rec *dputil.DataRecord) *OptodeData {
	if rec.Source != "optode_1" {
		return nil
	}
	// Save some typing
	pf32 := proto.Float32

	msg := &OptodeData{Time: proto.Uint64(uint64(rec.T.UnixNano()) / 1000)}
	dr := rec.Data.(map[string]interface{})
	msg.Doconcs = pf32(float32(dr["doconcs"].(float64)))
	msg.T = pf32(float32(dr["t"].(float64)))

	return msg
}

// Create a new DataRecord from an AcmData message
func (rec *AcmData) ToDataRecord() *dputil.DataRecord {
	s := make(map[string]interface{})
	s["va"] = rec.GetVa()
	s["vb"] = rec.GetVb()
	s["vc"] = rec.GetVc()
	s["vd"] = rec.GetVd()
	s["hx"] = rec.GetHx()
	s["hy"] = rec.GetHy()
	s["hz"] = rec.GetHz()
	s["tx"] = rec.GetTx()
	s["ty"] = rec.GetTy()

	// Convert microseconds to seconds and nanoseconds
	t := int64(rec.GetTime())
	tstamp := time.Unix(t/1000000, (t%1000000)*1000)

	return &dputil.DataRecord{Source: "acm_1", T: tstamp, Data: s}
}

// Create a new AcmData message from an "acm" DataRecord
func AcmFromDataRecord(rec *dputil.DataRecord) *AcmData {
	if rec.Source != "acm_1" {
		return nil
	}
	// Save some typing
	pf32 := proto.Float32

	msg := &AcmData{Time: proto.Uint64(uint64(rec.T.UnixNano()) / 1000)}
	dr := rec.Data.(map[string]interface{})

	msg.Va = pf32(float32(dr["va"].(float64)))
	msg.Vb = pf32(float32(dr["vb"].(float64)))
	msg.Vc = pf32(float32(dr["vc"].(float64)))
	msg.Vd = pf32(float32(dr["vd"].(float64)))
	msg.Hx = pf32(float32(dr["hx"].(float64)))
	msg.Hy = pf32(float32(dr["hy"].(float64)))
	msg.Hz = pf32(float32(dr["hz"].(float64)))
	msg.Tx = pf32(float32(dr["tx"].(float64)))
	msg.Ty = pf32(float32(dr["ty"].(float64)))

	return msg
}

// Create a new DataRecord from an FlntuData message
func (rec *FlntuData) ToDataRecord() *dputil.DataRecord {
	s := make(map[string]interface{})
	s["chlaflo"] = rec.GetChlaflo()
	s["ntuflo"] = rec.GetNtuflo()

	// Convert microseconds to seconds and nanoseconds
	t := int64(rec.GetTime())
	tstamp := time.Unix(t/1000000, (t%1000000)*1000)

	return &dputil.DataRecord{Source: "flntu_1", T: tstamp, Data: s}
}

// Create a new FlntuData message from an "flntu" DataRecord
func FlntuFromDataRecord(rec *dputil.DataRecord) *FlntuData {
	if rec.Source != "flntu_1" {
		return nil
	}
	// Save some typing
	pi32 := proto.Int32

	msg := &FlntuData{Time: proto.Uint64(uint64(rec.T.UnixNano()) / 1000)}
	dr := rec.Data.(map[string]interface{})

	switch t := dr["chlaflo"].(type) {
	case int64:
		msg.Chlaflo = pi32(int32(t))
	case uint64:
		msg.Chlaflo = pi32(int32(t))
	}

	switch t := dr["ntuflo"].(type) {
	case int64:
		msg.Ntuflo = pi32(int32(t))
	case uint64:
		msg.Ntuflo = pi32(int32(t))
	}

	return msg
}

// Create a new DataRecord from an FlcdData message
func (rec *FlcdData) ToDataRecord() *dputil.DataRecord {
	s := make(map[string]interface{})
	s["cdomflo"] = rec.GetCdomflo()

	// Convert microseconds to seconds and nanoseconds
	t := int64(rec.GetTime())
	tstamp := time.Unix(t/1000000, (t%1000000)*1000)

	return &dputil.DataRecord{Source: "flcd_1", T: tstamp, Data: s}
}

// Create a new FlcdData message from an "flcd" DataRecord
func FlcdFromDataRecord(rec *dputil.DataRecord) *FlcdData {
	if rec.Source != "flcd_1" {
		return nil
	}
	// Save some typing
	pi32 := proto.Int32

	msg := &FlcdData{Time: proto.Uint64(uint64(rec.T.UnixNano()) / 1000)}
	dr := rec.Data.(map[string]interface{})

	switch t := dr["cdomflo"].(type) {
	case int64:
		msg.Cdomflo = pi32(int32(t))
	case uint64:
		msg.Cdomflo = pi32(int32(t))
	}

	return msg
}

// Create one or more new DataRecords from a SensorData message
func (cont *SensorData) ToDataRecords() []*dputil.DataRecord {
	recs := make([]*dputil.DataRecord, 0)

	if rec := cont.GetCtd(); rec != nil {
		recs = append(recs, rec.ToDataRecord())
	}
	if rec := cont.GetOptode(); rec != nil {
		recs = append(recs, rec.ToDataRecord())
	}
	if rec := cont.GetAcm(); rec != nil {
		recs = append(recs, rec.ToDataRecord())
	}
	if rec := cont.GetFlntu(); rec != nil {
		recs = append(recs, rec.ToDataRecord())
	}
	if rec := cont.GetFlcd(); rec != nil {
		recs = append(recs, rec.ToDataRecord())
	}

	return recs
}

// Create a new SensorData message from one or more DataRecords
func SensorFromDataRecords(recs []*dputil.DataRecord) *SensorData {
	sd := &SensorData{}
	for _, rec := range recs {
		switch rec.Source {
		case "ctd_1":
			sd.Ctd = CtdFromDataRecord(rec)
		case "optode_1":
			sd.Optode = OptodeFromDataRecord(rec)
		case "acm_1":
			sd.Acm = AcmFromDataRecord(rec)
		case "flntu_1":
			sd.Flntu = FlntuFromDataRecord(rec)
		case "flcd_1":
			sd.Flcd = FlcdFromDataRecord(rec)
		}
	}

	return sd
}

// Create a new ProfileMessage struct from a Profile struct.  This is
// the internal format used on the DPC.
//
// The ProfileMessage struct will be stored in a queue, the second return
// value defines how the struct should be added to the queue; replace,
// prepend, or append, and the third value is the pattern name.
func (p *Profile) ToProfileMessage() (*ProfileMessage, Profile_Queueing, string) {
	// Build the ProfileConfig struct
	cfg := mpc.ProfileConfig{
		StartTime:  int64(p.GetTstart()),
		StopCheck:  5,
		ShallowErr: 3,
		DeepErr:    3,
		TimeLimit:  uint16(p.GetMaxtime()),
		Requeue:    p.GetRequeue(),
	}
	switch p.GetDir() {
	case Profile_DIVE:
		cfg.Direction = mpc.PROFILE_DOWN
		cfg.Deep = uint16(p.GetDepth())
		cfg.Shallow = 0
	case Profile_RISE:
		cfg.Direction = mpc.PROFILE_UP
		cfg.Shallow = uint16(p.GetDepth())
		cfg.Deep = 6000
	case Profile_STATIONARY:
		cfg.Direction = mpc.PROFILE_STATIONARY
	case Profile_DOCKING:
		cfg.Direction = mpc.PROFILE_DOCKING
	}

	msg := ProfileMessage{Cfg: &cfg, Exclude: p.GetExclude()}

	return &msg, p.GetQueueing(), p.GetPattern()
}

// Create a Profile struct from a map of empty interfaces. The primary
// use of this function is to allow the use of strings for enum values.
func ProfileFromInterface(raw map[string]interface{}) *Profile {
	p := Profile{}
	var (
		dir  Profile_ProfileType
		qing Profile_Queueing
	)

	p.Exclude = make([]string, 0)

	for key, val := range raw {
		switch key {
		case "dir":
			switch v := val.(type) {
			case string:
				x, ok := Profile_ProfileType_value[v]
				if !ok {
					return nil
				}
				dir = Profile_ProfileType(x)
			case int:
				dir = Profile_ProfileType(v)
			}
			p.Dir = &dir
		case "queueing":
			switch v := val.(type) {
			case string:
				x, ok := Profile_Queueing_value[v]
				if !ok {
					return nil
				}
				qing = Profile_Queueing(x)
			case int32:
				qing = Profile_Queueing(v)
			}
			p.Queueing = &qing
		case "exclude":
			for _, e := range val.([]interface{}) {
				p.Exclude = append(p.Exclude, e.(string))
			}
		case "depth":
			p.Depth = proto.Int32(int32(val.(int)))
		case "maxtime":
			p.Maxtime = proto.Int32(int32(val.(int)))
		case "tstart":
			p.Tstart = proto.Int32(int32(val.(int)))
		case "requeue":
			p.Requeue = proto.Uint32(uint32(val.(int)))
		case "pattern":
			p.Pattern = proto.String(val.(string))
		}
	}

	return &p
}

func (b *Battery) ToDataRecord() *dputil.DataRecord {
	s := make(map[string]interface{})
	s["count"] = b.GetCount()
	s["temp"] = b.GetTemp()
	s["voltage"] = b.GetVoltage()
	s["current"] = b.GetCurrent()
	s["energy"] = b.GetEnergy()
	s["rel_charge"] = b.GetRelCharge()

	// Convert microseconds to seconds and nanoseconds
	t := int64(b.GetTime())
	tstamp := time.Unix(t/1000000, (t%1000000)*1000)

	return &dputil.DataRecord{Source: "battery", T: tstamp, Data: s}
}

// Create a Battery struct from a map of empty interfaces.
func BatteryFromInterface(raw map[string]interface{}) *Battery {
	b := Battery{}

	for k, v := range raw {
		switch k {
		case "t":
			b.Time = proto.Uint64(uint64(v.(int64) * 1000000))
		case "count":
			b.Count = proto.Int32(int32(v.(int64)))
		case "temp":
			b.Temp = proto.Int32(int32(v.(int64) / 100))
		case "voltage":
			b.Voltage = proto.Int32(int32(v.(int64)))
		case "current":
			b.Current = proto.Int32(int32(v.(int64)))
		case "energy":
			b.Energy = proto.Int32(int32(v.(int64)))
		case "rel_charge":
			b.RelCharge = proto.Int32(int32(v.(int64)))
		}
	}

	return &b
}
