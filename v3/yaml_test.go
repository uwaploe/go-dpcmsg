package dpcmsg

import (
	"testing"

	yaml "gopkg.in/yaml.v3"
)

var input = `
- dir: DIVE
  depth: 5
  err: 1
  maxtime: 60
  tstart: 600
  exclude:
    - acs_1
  pattern: basic
- &P
  dir: RISE
  depth: 2
  maxtime: 60
  tstart: 600
  pattern: basic
- dir: 2
  maxtime: 180
- dir: DIVE
  depth: 5
  maxtime: 60
  tstart: 600
  pattern: basic
- dir: DOCKING
`

func TestYaml(t *testing.T) {
	var (
		prfs []Profile
	)

	prfs = make([]Profile, 0)
	if err := yaml.Unmarshal([]byte(input), &prfs); err != nil {
		t.Fatal(err)
	}

	if q := prfs[0].GetQueueing(); q != Profile_APPEND {
		t.Errorf("Bad queueing value: %v", q)
	}

	if perr := prfs[0].GetErr(); perr != 1 {
		t.Errorf("Bad pressure error value; expected 1, got %v", perr)
	}

	if prfs[0].GetDir() != Profile_DIVE {
		t.Errorf("Decode error: %v\n", &prfs[0])
	}
	if prfs[0].GetPattern() != "basic" {
		t.Errorf("Decode error: %v\n", &prfs[0])
	}

	if prfs[2].GetDir() != Profile_STATIONARY {
		t.Errorf("Decode error: %v\n", &prfs[2])
	}

	if prfs[2].GetMaxtime() != 180 {
		t.Errorf("Decode error: %v\n", &prfs[2])
	}
	if prfs[2].GetPattern() != "" {
		t.Errorf("Non-empty pattern: %v\n", &prfs[2])
	}

	if perr := prfs[2].GetErr(); perr != 0 {
		t.Errorf("Bad pressure error value; expected 0, got %v", perr)
	}
}
