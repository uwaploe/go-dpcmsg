module bitbucket.org/uwaploe/go-dpcmsg/v3

go 1.21.5

require (
	bitbucket.org/uwaploe/dpipc v0.1.0
	bitbucket.org/uwaploe/go-mpc v1.3.1
	google.golang.org/protobuf v1.27.1
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/vmihailenco/msgpack/v5 v5.4.1 // indirect
	github.com/vmihailenco/tagparser/v2 v2.0.0 // indirect
)
