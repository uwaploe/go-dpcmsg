package dpcmsg

import (
	"testing"
	"time"

	"bitbucket.org/uwaploe/dpipc"
	"google.golang.org/protobuf/proto"
)

const refDate = "Mon Jan 2 22:04:05 UTC 2006"
const refTimestamp = 1136239445000000

var status = &Status{
	Time:         refTimestamp,
	Profile:      42,
	Pressure:     (1234000),
	MotorCurrent: (123),
	State:        Status_StateType(Status_StateType_value["PROFILE_DOWN"]),
	Itemp:        []int32{123, 456, 789},
	Humidity:     (-1),
	Voltage:      (12000),
	Current:      (-456),
	Energy:       (987654),
	RelCharge:    (55),
	Vmon:         (11600),
	Duty:         (80),
	Ipr:          (147),
}

func TestSensorConvert(t *testing.T) {
	table := []struct {
		in  dpipc.DataRecord
		out proto.Message
	}{
		{
			in: dpipc.DataRecord{
				Src: "ctd_1",
				Data: map[string]interface{}{"condwat": float64(33.1),
					"tempwat": float32(7.8), "preswat": float64(1012)}},
			out: &SensorData{Ctd: []*CtdData{{
				Time:    refTimestamp,
				Condwat: 331000, Tempwat: 7800, Preswat: 1012000}}},
		},
		{
			in: dpipc.DataRecord{
				Src: "ctd_1",
				Data: map[string]interface{}{"condwat": float64(33.1),
					"tempwat": float32(7.8), "preswat": float64(-0.15)}},
			out: &SensorData{Ctd: []*CtdData{{
				Time:    refTimestamp,
				Condwat: 331000, Tempwat: 7800, Preswat: -150}}},
		},
		{
			in: dpipc.DataRecord{
				Src: "flntu_1",
				Data: map[string]interface{}{
					"chlaflo": int64(42),
					"ntuflo":  uint64(1234)}},
			out: &SensorData{Flntu: []*FlntuData{{
				Time:    refTimestamp,
				Chlaflo: 42, Ntuflo: 1234}}},
		},
		{
			in: dpipc.DataRecord{
				Src: "acm_1",
				Data: map[string]interface{}{
					"va": float32(1.2), "vb": float64(2.4), "vc": float32(4.8), "vd": float32(-8.16),
					"hx": float32(0.3), "hy": float64(0.25), "hz": float64(-0.93),
					"tx": float64(-1), "ty": float64(1)}},
			out: &SensorData{Acm: []*AcmData{{
				Time: refTimestamp,
				Va:   120, Vb: 240, Vc: 480, Vd: -816, Tx: -100, Ty: 100,
				Hx: 30, Hy: 25, Hz: -93}}},
		},
		{
			in: dpipc.DataRecord{
				Src: "optode_1",
				Data: map[string]interface{}{
					"doconcs": float32(98.6), "t": float64(12.34)}},
			out: &SensorData{Optode: []*OptodeData{{
				Time:    refTimestamp,
				Doconcs: 9860, T: 1234}}},
		},
		{
			in: dpipc.DataRecord{
				Src: "battavg",
				Data: map[string]interface{}{
					"count": uint32(10), "capacity": uint32(92),
					"voltage": float32(16.125), "amperage": float32(3.456)}},
			out: &SensorData{Batt: []*BatterySummary{{
				Time:  refTimestamp,
				Count: 10, Capacity: 92,
				Voltage: 16125, Amperage: 3456}}},
		},
	}

	for _, e := range table {
		e.in.T, _ = time.Parse(time.UnixDate, refDate)
		sd := SensorFromDataRecords([]dpipc.DataRecord{e.in})
		t.Logf("%s message size: %d bytes", e.in.Src, proto.Size(sd))
		if !proto.Equal(sd, e.out) {
			t.Errorf("Bad conversion; expected %#v, got %#v", e.out, sd)
		}
	}
}

func TestMarshalStatus(t *testing.T) {
	dr := status.MarshalDataRecord()

	if dr.Src != "profiler" {
		t.Errorf("Bad data-record source field: %v", dr.Src)
	}

	h, m, s := dr.T.UTC().Clock()
	if h != 22 || m != 4 || s != 5 {
		t.Errorf("Time-stamp decode error: %v", dr.T)
	}

	data, ok := dr.Data.(map[string]interface{})
	if !ok {
		t.Errorf("Bad type for Data field: %v", dr.Data)
	}

	rh, ok := data["humidity"].(int32)
	if !ok {
		t.Errorf("Bad type for Humidity field: %v", data["humidity"])
	}

	if rh != -1 {
		t.Errorf("Bad value for Humidity field: %v", rh)
	}

	vmon, ok := data["vmon"].(int32)
	if !ok {
		t.Errorf("Bad type for Vmon field: %v", data["vmon"])
	}

	if vmon != 11600 {
		t.Errorf("Bad value for Vmon field: %v", rh)
	}

	state, ok := data["state"].(string)
	if !ok {
		t.Errorf("Bad type for State field: %v", data["state"])
	}

	if state != "profile_down" {
		t.Errorf("Bad value for State field: %v", data["state"])
	}
}

func TestMmpData(t *testing.T) {
	mmp_dr := dpipc.DataRecord{
		Src: "mmp_1",
		Data: map[string]interface{}{
			"pnum":     int64(42),
			"mode":     "profile_down",
			"pressure": float64(1234.0),
			"current":  int64(123),
			"speed":    0.25,
			"duty":     50,
		},
	}
	mmp_dr.T, _ = time.Parse(time.UnixDate, refDate)

	expected := &Status{
		Time:         refTimestamp,
		MpcTime:      refTimestamp / 1000000,
		Profile:      42,
		State:        Status_StateType(Status_StateType_value["PROFILE_DOWN"]),
		Pressure:     1234000,
		MotorCurrent: 123,
		Speed:        250,
		Duty:         50,
	}

	s := &Status{}
	err := s.UnmarshalDataRecord(mmp_dr)
	if err != nil {
		t.Fatal(err)
	}

	if !proto.Equal(s, expected) {
		t.Errorf("Bad conversion; expected %#v, got %#v", expected, s)
	}
}
