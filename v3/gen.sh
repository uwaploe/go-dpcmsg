#!/usr/bin/env bash

PKG="dpcmsg"

set -e
protoc -I=. --go_out=. --go_opt=paths=source_relative \
       --go-vtproto_out=. \
       --go-vtproto_opt=features=marshal+unmarshal+size ${PKG}.proto
mv -v bitbucket.org/uwaploe/${PKG}/${PKG}_vtproto.pb.go .
rm -rf bitbucket.org
